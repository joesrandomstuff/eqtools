# This will merge the Friends of all Everquest/Project1999 Characters and
# write them to all characters.
# Has to be run in your EQ Directory and can be integrated into the launch bat.

#!/usr/bin/env python3

import os, sys, glob, re, configparser
import logging as log

log.basicConfig(encoding='utf-8', level=log.INFO)

if os.path.isfile("mergefriends.ini"):
    log.info("mergefriends.ini config file found, loading content.")
    appconfig = configparser.ConfigParser()
    appconfig.read("mergefriends.ini")
    try:
        if appconfig['defaults']['debuglog'] == "true":
            log.getLogger().setLevel(log.DEBUG)
            log.debug("Debug log on.")
        else:
            log.info("Debug log off.")
    except KeyError:
        log.info("Debug log off.")
        pass
    ADDFRIENDS = []
    REMOVEFRIENDS = []
    try:
        for friend in appconfig['friends']:
            if appconfig['friends'][friend] == "add":
                ADDFRIENDS.append(friend)
            if appconfig['friends'][friend] == "remove":
                REMOVEFRIENDS.append(friend)
    except KeyError:
        pass
else:
    log.info("No mergefriends.ini config file found, continuing without.")

if len(REMOVEFRIENDS) > 0:
    log.debug("Friends to be removed: " + str(len(REMOVEFRIENDS)) + " " + str(REMOVEFRIENDS))
else:
    log.debug("No friends to be removed.")

if not os.path.isfile("eqclient.ini"):
    print("eqclient.ini not found in the current directory. Please run this from your EQ installation directory.")
    sys.exit(1)
log.debug("eqclient.ini was found in the current directory, so it's assumed to be running at the correct location.")

CHARACTERFILES = [f for f in glob.glob("*_*.ini") if re.match('(?!^UI_).*',os.path.basename(f))]
log.debug("Characterfiles found: " + str(CHARACTERFILES))

MASTERLIST = []
for characterfile in CHARACTERFILES:
    log.debug("Reading: " + characterfile)
    config = configparser.ConfigParser()
    config.read(characterfile)
    for friendnum in config['Friends']:
        log.debug("Checking " + friendnum + ": " + config['Friends'][friendnum])    
        if config['Friends'][friendnum] != "*NULL*":
            MASTERLIST.append(config['Friends'][friendnum])
log.debug("Gathered " + str(len(MASTERLIST)) + " names.")

CLEANMASTERLIST = []
for name in MASTERLIST:
    if name.lower() not in CLEANMASTERLIST:
	    CLEANMASTERLIST.append(name.lower())
log.debug("Uniquified to " + str(len(CLEANMASTERLIST)) + " friends: " + str(CLEANMASTERLIST))

for friend in ADDFRIENDS:
    if friend.lower() not in CLEANMASTERLIST:
        CLEANMASTERLIST.append(friend.lower())
for friend in REMOVEFRIENDS:
    if friend.lower() in CLEANMASTERLIST:
        CLEANMASTERLIST.remove(friend.lower())
log.debug("Friend count after add/removal: " + str(len(CLEANMASTERLIST)) + " " + str(CLEANMASTERLIST))

if len(CLEANMASTERLIST) > 100:
    log.error("Total friend count is over 100 so these can not be written due to EQ limits. Terminating.")
    sys.exit(1)

CLEANMASTERLIST.sort(reverse=True)
for characterfile in CHARACTERFILES:
    config = configparser.ConfigParser()
    config.read(characterfile)
    counter = 99
    for friend in CLEANMASTERLIST:
        config['Friends']["friend" + str(counter)] = friend
        counter -=1
    while counter >= 0:
        config['Friends']["friend" + str(counter)] = "*NULL*"
        counter -=1
    with open(characterfile, 'w') as configfile:
        config.write(configfile, space_around_delimiters=False)
log.debug("Characterfiles written.")
