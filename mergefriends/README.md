# Eqtools

Some tools for the Everquest / Project1999 client. See subdirectories for details.
Requires python3.

## Mergefriends

Merge all your character's friends into one list and distribute it to all your characters.

### Usage

Put mergefriends.py (and .ini) into your EQ directory, run it manually the way you choose or integrate it into e.g. Launch Titanium.bat for automatic use.

### Configuration

A mergefriends.ini file can optionally be used. It contains the following options:

`[defaults]`
`debuglog=true/false`
True turns on the debug logging.

`[friends]`
`name1=add`
`name2=remove`
Any name with "add" will be added while every name with remove will be removed from the merged list of friends *for every character*.
